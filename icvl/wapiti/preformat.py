#############################################
# File: preformat.py
# Author: Gael Marcadet
# Description: Prepares wapiti for the training
# 	and for the testing steps.
#############################################

import random
import os
import re
import xml.etree.ElementTree as ET

SNT_FILE_REGEX = ".*.snt$"
DATA_FOLDER="../../data/IOBCorpus"
OUTPUT_TRAIN_FILENAME="train.snt"
OUTPUT_TEST_FILENAME="test.snt"
TEST_SIZE=0.33

def load_data( folder_name ):
	data = []
	labels = set()
	labels_freq = {}
	for dir, folders, files in os.walk( folder_name ):
		files.sort()
		for filename in files:
			if re.match( SNT_FILE_REGEX, filename ):
				print( "Parsing file " + dir + "/" + filename  )
				location = dir + "/" + filename
				with open( location, "r" ) as file:
					for line in file.readlines():
						# clear the line
						line = line.replace("{S}", "").replace("\n", "").lower()

						# if line starting with '<' character, it's highly probably an XML line
						if line.startswith('<'):
							try:
								root = ET.fromstring( line )
								words = root.text.split( " " )
								label = "i-" + root.tag
							except:
								print( "XML Parsing failed: Bad-formed line: ", line )
						else:	 
							tokens = line.split()
							words = tokens[:-1]
							label = tokens[-1]
						labels.add(label)
						if label in labels_freq:
							labels_freq[label] += 1
						else:
							labels_freq[label] = 0

						for word in words:
							data.append( (word, label) )

	print( "There are", len(data), "words" )
	print( "There are", len(labels), "labels:", labels )
	print( labels_freq )
	return data
				
def split_train_test( data, test_size ):
	nlines = len(data)
	test_nlines = int(nlines * test_size)
	train_nlines = nlines - test_nlines
	data_train = data[:train_nlines]
	data_test = data[train_nlines:]
	return data_train, data_test

def _is_punct( token ):
	return token in [ ".", "!", "?" ]

if __name__ == "__main__":
	# loads data from corpus
	# and split data into two sets for the training and testing steps
	data = load_data( DATA_FOLDER )
	train, test = split_train_test( data, test_size=TEST_SIZE )
	print( "There are", len(train), "words in train" )
	
	print( "Creating output train file called ", OUTPUT_TRAIN_FILENAME, "..." )
	with open( OUTPUT_TRAIN_FILENAME, 'w' ) as file:
		for w, l in train:
			#file.write(f"{w} {l}\n")
			file.write( f"{w} {l}\n" + ( "\n" if _is_punct( w ) and l == "o"  else "" ) )

	# IMPORTANT: At this step, we ignore labels, we want predictions !
	print( "There are", len(test), "words in test" )
	print( "Creating output test file called ", OUTPUT_TEST_FILENAME, "..." )
	with open( OUTPUT_TEST_FILENAME, "w" ) as file:
		for w, l in test:
			file.write( f"{w} {l}\n" + ( "\n" if _is_punct( w ) and l == "o" else "" ) )
	