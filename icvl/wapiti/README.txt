Contient tous les fichiers nécessaires pour l'exécution de Wapiti.

============ Installation ==============
Pour installer Wapiti, aller sur la page officiel https://wapiti.limsi.fr/#download
ou bien sur la page GitHub https://github.com/Jekub/Wapiti et suivre la procédure d'installation.
