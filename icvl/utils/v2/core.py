##########################"
# File: utils2.py
# Author: Gael Marcadet
# Description: New utils version.
##########################

import os
import re
import random
import xml.etree.ElementTree as ET
from icvl.utils.v2.encoders import LabelEncoder, TokenEncoder, AbstractEncoder
from tensorflow.keras.preprocessing.sequence import pad_sequences
from sklearn.preprocessing import OneHotEncoder, StandardScaler
from functools import partial
import numpy as np
from sklearn.model_selection import train_test_split

def flatten( data ):
	res = []
	def _local_flatten( data ):
		if isinstance( data, list ):
			for item in data:
				_local_flatten( item )
		else:
			res.append( data )
	_local_flatten( tolist(data) )
	return res

def tolist( array ):
	if isinstance( array, list ):
		return array
	else:
		try:
			return [ i for i in array ]
		except:
			return [array]

def is_punct( token ):
	''' Returns True if token is punctation, False otherwise. '''
	return token in [ ".", "?", "!" ]

SNT_FILE_REGEX = ".*.snt$"

def split_train_test( X, y, test_size ):
	
	nlines = len(X)
	test_nlines = int(nlines * test_size)
	train_nlines = nlines - test_nlines
	X_train, X_test = X[:train_nlines], X[train_nlines:]
	y_train, y_test = y[:train_nlines], y[train_nlines:]
	return X_train, y_train, X_test, y_test


def export_encoder( filename :str, encoder : AbstractEncoder ):
	''' Exports an encoder on specified file. '''
	with open( filename, "w" ) as file:
		for token, identifier in encoder.tokens_id:
			file.write( f"{identifier}:{token}" )

def initalize_encoder( filename : str, encoder : AbstractEncoder ) -> AbstractEncoder:
	''' Initialize an encoder from specified file. '''
	data = {}
	with open( filename, "r" ) as file:
		for line in file.readlines():
			identifier, token = line.split(':',maxsplit=1)
			data[token] = identifier
	encoder.id = len(data)
	encoder.tokens_id = data
	return encoder


def _test_system_on_file( file_location ):
	''' Tests loading system to check if all NE are detected. '''
	# the first step is to load all NE from custom system
	loaded = []
	with open( file_location, "r" ) as file:
		sequences, labels = [[]], [[]]
		token_encoder = TokenEncoder()
		label_encoder = LabelEncoder()
		_load_text_file( file, sequences, labels, token_encoder, label_encoder )
		for seq, lab in zip( sequences, labels ):
			for word, label in zip( seq, lab ):
				if label != 0:
					loaded.append((token_encoder.invert_transform_single_token(word), label_encoder.invert_transform_single_token(label)))
	
	# Then we retreive the NE again but with an XML parser, which is efficency to retreive all NE 
	# well-formated !
	real = []
	with open( file_location, "r" ) as file:
		text=  " ".join(file.readlines())
		root = ET.fromstring( "<text>" +  _clean_text(text) + "</text>" )
		for child in root:
			real.append( (child.text, child.tag) )
	

	print( "Homemade parser: #EN:", len(loaded) )
	print( "XML parser: ", len(real) )
	for w, l in loaded:
		print( w, l )

def _frequencies_and_repartition( data ):
	''' Returns a dict where the key is an unique value from data and the value is
	a tuple of two values, the frequency and the repartition of the key. '''
	freq = {}
	def local( data ):
		if isinstance( data, np.ndarray ):
			return local(data.tolist())
		if isinstance(data[0], list):
			for item in data:
				local(item)
		else:
			for val in data:
				if val in freq:
					freq[val] += 1
				else:
					freq[val] = 1
	local( data )
	total = sum(freq.values())
	return { l: (n, n/total) for l,n in freq.items() }



def _summary_labels_repartition( train, test, labels_encoder : LabelEncoder ):
	''' Displays a summary about labels. '''
	def _summary_repartition( data, title ):
		print( f"\t* {title}" )
		# computes labels frequencies and repartition
		freq_repa = _frequencies_and_repartition( data )
		for label, ( frequency, repartition ) in freq_repa.items():
			print( f"\t\t- {labels_encoder.invert_transform_single_token(label)}: Frequency = {frequency}, \tRepartition = {repartition}" )

	print( "* Labels Repartition" )
	_summary_repartition( train, "Train" )
	_summary_repartition( test, "Test" )
			


class Dataset:
	def __init__(self, train, test, tokens_encoder, labels_encoder, scaler):
		self.train = train
		self.test = test
		self.tokens_encoder : TokenEncoder = tokens_encoder
		self.labels_encoder : LabelEncoder = labels_encoder
		self.scaler = scaler
	
	def summary( self ):
		train_nobs = len(self.train[0])
		test_nobs = len(self.test[0])
		total_nobs = train_nobs + test_nobs
		print( "-" * 50 )
		print( "* # Observations: ", total_nobs )
		print( "\t# train observations: \t", train_nobs )
		print( "\t# test observations: \t", test_nobs )
		print( "\tTrain ratio: \t", train_nobs / total_nobs )
		print( "\t# Classes:", len(self.labels_encoder.classes()) )
		print( "\t# Different words:", len( self.tokens_encoder.classes() ) )
		for label in self.labels_encoder.classes():
			print( "\t\t-", label )

		print( "* Shapes" )
		print( "\tInput shape: \t", self.train[0].shape )
		print( "\tLabel shape: \t", self.train[1].shape )

		print( "* Samples" )
		print( "\tInput sample: \t", self.train[0][:2] )
		print( "\tLabel sample: ", self.train[1][:2] )
		

		_summary_labels_repartition( self.train[1], self.test[1], labels_encoder=self.labels_encoder )

		print( "-" * 50 )

	
def _clean_text( file_content ):
	''' Clean and returns a text. '''
	# remove IOB FORMAT content
	#file_content = re.sub( "[A-Z]-[A-Z]+", "", file_content )

	# remove unwanted chevron which can be interpreted as xml
	file_content = re.sub( "(<[^/A-Z]|[^A-Z]>)", "", file_content )
	# pre-process content
	file_content = file_content.lower()
	return file_content

def _parse_xml( xml_item, raise_error=True ):
	''' Parse an xml item to extract the text and his label. '''
	try:
		root = ET.fromstring( xml_item )
		words = root.text.split( " " )
		label = "i-" + root.tag.lower()
		return words, label
	except ET.ParseError as e:
		print( "XML parsing failure on xml item :", xml_item )
		if raise_error:
			exit( 1 )

def _load_snt_file( file, sentences, labels, tokens_encoder : TokenEncoder, labels_encoder : TokenEncoder ):
	for line in file.readlines():
		# clear the line
		line = line.replace("{S}", "").replace("\n", "").lower()

		# if line starting with '<' character, it's highly probably an XML line
		if line.startswith('<'):
			try:
				root = ET.fromstring( line )
				words = root.text.split( " " )
				label = "i-" + root.tag
			except:
				print( "XML Parsing failed: Bad-formed line: ", line )
		else:	 
			tokens = line.split()
			words = tokens[:-1]
			label = tokens[-1]
		
		

		for word in words:
			# insert the word in sentences
			labels_encoder.add( label )
			labels[-1].append( labels_encoder.transform_single_token(label) )

			tokens_encoder.add(word)
			sentences[-1].append( tokens_encoder.transform_single_token(word) )

			# if word is a dot, go to the next line
			if is_punct( word ):
				sentences.append([])
				labels.append([])

def _is_iob_label( token ):
	return token in [
		"o", "i-lieu", "i-org", "i-pers"
	]

def _load_text_file( file, sentences, labels, tokens_encoder : TokenEncoder, labels_encoder : TokenEncoder ):
	token_start = "<==" 
	inside_xml = False
	buffer = ""

	file_content = " ".join(file.readlines() )
	# if document starts with "==> ... <==", skip this part 
	token_start_index = file_content.find( token_start )
	if token_start_index != -1:
		token_start_index = token_start_index + len(token_start)
		file_content = file_content[token_start_index:]
	file_content = _clean_text( file_content )
	
	

	# document parsing
	words = file_content.split()
	for word in words:
		starts_as_xml = word[0] == "<"
		ends_as_xml = word[-1] == ">"

		if starts_as_xml and ends_as_xml:
			words, label = _parse_xml( word )
		elif starts_as_xml:
			inside_xml = True
			buffer = word
			continue
		elif ends_as_xml:
			if inside_xml:
				words, label = _parse_xml( buffer + " " + word )
				buffer = ""
				inside_xml = False
		else:
			if inside_xml:
				buffer = buffer + " " + word
				continue
			else:
				# change last word if current word is a label
				if _is_iob_label( word ):
					# delete label prefix
					label = word
					labels_encoder.add( label )
					label = labels_encoder.transform_single_token( label )

					# update the last word label
					# if a dot is labelized, a mistake has been made
					# indeed a new sentence was created and must be remove because the dot
					# not represents the end of a sentence but an element in an NE
					if labels[-1] == []:
						del sentences[-1]
						del labels[-1]
					labels[-1][-1] = label
					continue
				else:
					words, label = [word], "o"

		# insert the word in sentences
		labels_encoder.add( label )
		for word in words:
			tokens_encoder.add(word)
			sentences[-1].append( tokens_encoder.transform_single_token(word) )
			labels[-1].append( labels_encoder.transform_single_token(label) )
			# if word is a dot, go to the next line
			if is_punct( word ):
				sentences.append([])
				labels.append([])
	
	return sentences, labels, tokens_encoder, labels_encoder

				

def _load_sentences_from_corpus( corpus_directory, files_extension,  keep_prob = 1 ):
	""" 
	Loads and returns sentences located on a corpus.

	"""
	# defines if parser file where sentences are splitted in several lines 
	# or in a lambda text
	one_word_in_line = "snt" in files_extension
	if one_word_in_line:
		print( "Parsing file as one word by line" )
	else:
		print( "Parsing as standard encoder" )

	sentences = [ [] ]
	labels = [ [] ]
	labels_encoder = LabelEncoder()
	tokens_encoder = TokenEncoder()
	files_extension_regex = f".*.{files_extension}$"
	for dir, folders, files in os.walk( corpus_directory ):
		files.sort()
		for filename in files:
			if re.match( files_extension_regex, filename ) and random.random() <= keep_prob:
				print( "Parsing file " + dir + "/" + filename  )
				location = dir + "/" + filename
				with open( location, "r" ) as file:
					if one_word_in_line:
						_load_snt_file( file, sentences, labels, tokens_encoder, labels_encoder )
					else:
						_load_text_file( file, sentences, labels, tokens_encoder, labels_encoder )

					# checks data constistance
					for sentence, label in zip( sentences, labels ):
						if len(sentence) != len(label):
							for i in range(min(len(sentence), len(label))):
								if label[i] != 0:
									print( "=> ", tokens_encoder.invert_transform_single_token(sentence[i]), "as", labels_encoder.invert_transform_single_token(label[i]) )
							print( " ".join( tokens_encoder.invert_transform( sentence )) )
							print( " ".join( labels_encoder.invert_transform( label ) ) )
							raise Exception( f"Inconstent data: Not the same number of words and labels !" )

	print( "There are", sum(map(len, sentences)), "words" )
	return sentences, labels, tokens_encoder, labels_encoder




def load_data_as_sequences( corpus_directory, files_extension, stop_tag = "<STOP>", one_hot_encoding = False, test_size = 0.33, maxlen=None ):
	# the first steop is to load data as sequence from the corpus
	sentences, labels, tokens_encoder, labels_encoder = _load_sentences_from_corpus( corpus_directory, files_extension=files_extension )


	# then we must pads all sequences to get the same length
	tokens_encoder.add( stop_tag )
	stop_tag_id = tokens_encoder.transform_single_token( stop_tag )
	sentences = pad_sequences( sequences=sentences, padding="post", value=stop_tag_id, maxlen=maxlen )
	labels = pad_sequences( sequences=labels, padding="post", value=labels_encoder.transform_single_token("o"), maxlen=len(sentences[0]) ) 

	if one_hot_encoding:
		sentences = array2d_to_one_hot( sentences, vocab_size=len(tokens_encoder.classes()) )


	X_train, X_test, y_train, y_test = train_test_split( np.array(sentences), np.array(labels), test_size=test_size )
	
	# finally, we creates the dataset object
	dataset = Dataset( 
		(X_train, y_train),
		(X_test, y_test),
		tokens_encoder, 
		labels_encoder,
		scaler=None
		)
	
	return dataset



		

def load_data_as_words( corpus_directory, files_extension, one_hot_encoding = False, test_size=0.33, keep_prob = 1, normalize=False ):
	# the first steop is to load data as sequence from the corpus
	print( "* Loading data from corpus..." )
	sentences, labels, tokens_encoder, labels_encoder = _load_sentences_from_corpus( corpus_directory, files_extension, keep_prob=keep_prob )
	print( "* Transforming sentences to words..." )
	words, labels = flatten( sentences ), flatten( labels )

	scaler = None
	# changes word representation into one hot encoding
	if one_hot_encoding:
		print( "* Encoding words into one hot encoding" )
		sentences = array1d_to_one_hot( words, vocab_size=len(tokens_encoder.classes()) )
	# changes word rep	
	elif normalize:
		scaler = StandardScaler()
		words = scaler.fit_transform( np.array( words ).reshape( -1, 1 ) )

	# finally, we creates the dataset object
	print( "* Splitting words into train and test sets" )
	X_train, y_train, X_test, y_test = split_train_test( np.array(words), np.array(labels), test_size=test_size )

	dataset = Dataset( 
		(X_train, y_train),
		(X_test, y_test),
		tokens_encoder, 
		labels_encoder,
		scaler=scaler
		)
	return dataset

def number_to_one_hot( n, vocab_size ):
	oh =  [ 0 ] * vocab_size
	oh[n] = 1
	return oh

def array1d_to_one_hot( data, vocab_size ):
	#return list(map( partial( number_to_one_hot, vocab_size=vocab_size ), data ))
	return [[ 0 if li != value else 1 for li in range( vocab_size )] for value in data ]

def array2d_to_one_hot( data, vocab_size ):
	return list(map( partial( array1d_to_one_hot, vocab_size=vocab_size ), data ))
	
if __name__ == "__main__":
	if False:
		with open( "data/BNFCorpus/xea.H.io_csc.txt", "r" ) as file:
			sentences, labels = [[]], [[]]
			te = TokenEncoder()
			le = LabelEncoder()
			_load_text_file( file, sentences, labels, te, le )
			
	if True:
		if True:
			dataset = load_data_as_words( "data/BNFCorpus", files_extension="txt", test_size=0.33 )
			dataset.summary()
		else:
			_test_system_on_file( "/home/gael/Documents/University/M1/Stage/Documents/Code/data/BNFCorpus/xaq.L.i_csc.txt" )