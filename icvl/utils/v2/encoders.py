class AbstractEncoder:
	def __init__(self, tokens = []):
		self.tokens_id = { token: id for id, token in enumerate(tokens) }
		self.id_tokens = { id:token for (token, id) in self.tokens_id.items() }
		self.id = len(tokens)

	def add( self, data ):
		for token in ( data if isinstance(data, list) else [data] ):
			if token not in self.tokens_id:
				id = self.__get_and_increment()
				self.tokens_id[ token ] = id
				self.id_tokens[ id ] = token
		
	def __get_and_increment( self ):
		n = self.id
		self.id += 1
		return n
	
	def classes( self ):
		return list(self.tokens_id.keys())
	
	def transform_1d( self, data ):
		return list(map(self.transform_single_token, data))
	
	def transform_2d(self, data):
		return list(map(self.transform_1d, data))

	def transform_single_token( self, token ):
		if token in self.tokens_id:
			return self.tokens_id[token]
		else:
			raise ValueError( "Cannot transform single token: Unkown token", token )

	def invert_transform_single_token( self, id ):
		if id in self.id_tokens:
			return self.id_tokens[ id ]
		else:
			raise ValueError( "Cannot invert transform token: given identifier", id, "hasn't been affected" )

	def invert_transform( self, idx ):
		return list( map( self.invert_transform_single_token, idx ) )

class LabelEncoder(AbstractEncoder):
	pass


class TokenEncoder(AbstractEncoder):
	def __init__(self, unknow_tag = "<UNK>"):
		super().__init__(tokens=[unknow_tag])
		self.unkown_tag = unknow_tag
	
	def transform_single_token( self, token ):
		if token in self.tokens_id:
			return self.tokens_id[token]
		else:
			return self.tokens_id[self.unkown_tag]
	
	