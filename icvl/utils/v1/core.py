####################################################
#	File: utils.py
#	Version: 0.0.1
# 	Author: Gael Marcadet
#	Description: Utils functions
####################################################


import glob
import os
import re
import sklearn
from sklearn.preprocessing import LabelEncoder
from sklearn.model_selection import train_test_split
import xml.etree.ElementTree as ET
import random
from random import choices
from itertools import chain
import numpy as np
from utils_core.utils import flatten


SNT_EXTENSION = ".snt"
SNT_FILE_REGEX = ".*.snt$"
DEFAULT_EMBEDDINGS_FILE = "./models/embeddings/frwiki/frwiki_20180420_100d.txt"
OUT_LABEL="o"




class Vocab:
	'''
	Object wrapper to maintain a vocabulary.
	'''
	def __init__(self, unkown_tag, stop_tag):
		self.unknown_tag = unkown_tag
		self.stop_tag = stop_tag
		self.token_id = { unkown_tag: 0, stop_tag: 1 }
		self.__id = 2
		self.vocab_size = 2 # starting at 1 due to unkown token

	def add_token( self, token ):
		if token not in self.token_id:
			self.token_id[ token ] = self.__get_and_increment()
			self.vocab_size += 1

	def get_id( self, token ):
		if token in self.token_id:
			return self.token_id[ token ]
		return self.token_id[ self.unknown_tag ]

	def __get_and_increment(self):
		id = self.__id
		self.__id += 1
		return id



def load_embeddings_weights_matrix( vocab : Vocab, embeddings_file, embeddings_dims = 100 ):
	'''
	Loads embeddings and return the related matrix depending on the vocabulary.
	'''
	EMBEDDING_DIM = embeddings_dims
	EMBEDDING_WEIGHTS_FILE = embeddings_file

	# read and parse embeddings in file
	print( "Loading", EMBEDDING_WEIGHTS_FILE,"embeddings weights" )	
	embeddings_index = {}
	with open( EMBEDDING_WEIGHTS_FILE, 'r' ) as file:
		# ignore the first line
		for line in file.readlines()[1:]:
			try:
				# read line and clrear
				tokens = line.split()
				word = tokens[0].replace( '/', " " ).replace( ":", " " ).split()[-1].lower()
				values = tokens[-100:] # 100 right numbers are coefs
				coefs = np.asarray(values, dtype='float32')
				embeddings_index[word] = coefs
			except ValueError as e:
				print( "Error: ", e, ": ", line )
				exit( 1 )
			except AttributeError as e:
				print( "Error: ", e, ": ", line )
				exit( 1 )
	
	# builds embeddings matrix from weights
	print( "Building embeddings matrix" )
	found = 0
	embedding_matrix = np.random.rand( vocab.vocab_size, EMBEDDING_DIM)
	for word, i in vocab.token_id.items():
		embedding_vector = embeddings_index.get(word)
		if embedding_vector is not None:
			found += 1
			# words not found in embedding index will be all-zeros.
			embedding_matrix[i] = embedding_vector

	# displays words cover
	print( "Embeddings / Corpus words match: ", found / vocab.vocab_size * 100 )

	return embedding_matrix

	

class Dataset:
	'''
	Wraps informations about dataset and translater to move from a data representation to another.
	'''
	def __init__( self, train, test, vocab, n_seqs, dim_embeds, labels_encoder, embeddings_matrix ):
		self.train = train
		self.test = test
		self.vocab = vocab
		self.n_seqs = n_seqs
		self.dim_embeds = dim_embeds
		self.labels_encoder = labels_encoder
		self.n_labels = len(self.labels_encoder.classes_)
		self.embeddings_matrix = embeddings_matrix

class LabelFrequencies:
	'''
	Computes labels frequencies dynamically.
	'''
	def __init__(self):
		self.labels_freq = {}
	
	def add( self, label ):
		''' Adds a new label occurrence.'''
		if label in self.labels_freq:
			self.labels_freq[label] += 1
		else:
			self.labels_freq[label] = 0
		
	def frequencies(self):
		''' Returns labels frequency. '''
		return self.labels_freq
	
	def repartition(self):
		''' Returns labels repartition. '''
		total = sum([ occ for label, occ in self.labels_freq.items() ])
		return { label: occ / total for label, occ in self.labels_freq.items() }


def load_snt_by_sentences( folder_name, keep_prob : float = 0.40, embeds_file=DEFAULT_EMBEDDINGS_FILE, word_as_one_hot = False, return_as_sentence = True ):
	UNKNOWN_TOKEN_TAG = "<INC>"
	STOP_TAG = "<STOP>"

	def load_file( file, vocab : Vocab, sentences, labels, labels_set, label_frequencies : LabelFrequencies ):
		''' Parse given file and insert loaded content in existing data. '''
		for line in file.readlines():
			# preprocess line
			line = line.replace("\n", "").replace( "{S}", "" ).lower()

			words = []
			label = None
			failed = False
			# first try to parse as xml	
			try:
				root = ET.fromstring( line )
				words = root.text.split( " " )
				label = "i-" + root.tag
			except ET.ParseError:
				failed = True
				pass

			# try to parse a WORD LABEL format
			if failed:
				data = line.split( ' ' )
				words = data[:-1]
				label = data[-1]
	
			label_frequencies.add( label )
			labels_set.add( label )
			for word in words:
				vocab.add_token( word )
				sentences[-1].append( word )
				labels[-1].append( label )
				if word == ".":
					labels.append( [] )
					sentences.append( [] )
			
	# creates params for managing data
	labels_frequencies = LabelFrequencies()
	labels_set = set()
	labels = [ [] ]	
	vocab = Vocab( UNKNOWN_TOKEN_TAG, STOP_TAG )
	sentences = [ [] ]

	# walk directory and subdirectory to load each file with a specific extension 
	# All files are picked in a probability defined above
	for dir, folders, files in os.walk( folder_name ):
		for filename in files:
			if re.match( SNT_FILE_REGEX, filename ) and random.random() <= keep_prob:
				location = dir + "/" + filename
				with open( location, "r" ) as file:
					load_file( file, vocab, sentences, labels, labels_set, labels_frequencies )

	# encodes all label to put them between 0 and n
	# where n is the number of labels
	label_encoder = LabelEncoder()
	label_encoder.fit( list( labels_set ) )
	
	if return_as_sentence:
		# padds sentence
		stop_id = vocab.get_id( vocab.stop_tag )
		print( len( sentences ), sentences[0], len(sentences[0]) )
		max_sentence_size = max( [ len( s ) for s in sentences ] )
		X = []
		y = []
		for s, l in zip( sentences, labels ):
			padding =  max_sentence_size - len( s )
			s_id = [ vocab.get_id( w ) for w in s ] + [ stop_id ] * padding
			X.append( s_id )
			y.append( label_encoder.transform( l + [ OUT_LABEL ] * padding ))
	else:
		X = []
		print( vocab.vocab_size )
		for s in sentences:
			for w in s:
				X.append( vocab.get_id( w ) )
		y = label_encoder.transform(list(flatten(labels)))
	
	# if word requested as one hot representation, transform it
	# initially, represented as integer
	if word_as_one_hot:
		if return_as_sentence:
			X_prev = X
			X = np.zeros(shape=(len(X_prev), max_sentence_size, vocab.vocab_size), dtype='float16')
			for seq_index, seq in enumerate(X_prev):
				for w_seq_pos, w_voc_pos in enumerate(seq):
					X[seq_index, w_seq_pos, w_voc_pos] = 1
		else:
			X_prev = X
			X = np.zeros(shape=(len(X_prev), vocab.vocab_size), dtype='float16')
			for word_pos_index, word_pos_voc in enumerate(X_prev):
				X[word_pos_index, word_pos_voc] = 1



	X = np.array( X )
	y = np.array( y )
	X_train, X_test, y_train, y_test = train_test_split( X, y )
	train = X_train, y_train
	test = X_test, y_test
	

	# loads embeddings weights
	if embeds_file:
		embeddings_weight_matrix = load_embeddings_weights_matrix( vocab, embeddings_file=embeds_file )
	else:
		embeddings_weight_matrix = None

	print( "-" * 30 )
	print( "Labels frequences: ", labels_frequencies.repartition() )
	print( "Vocab size: ", vocab.vocab_size )
	print( "# Sentences: ", len( X_train ) + len(X_test) )
	print( "# inputs:", max_sentence_size if return_as_sentence else vocab.vocab_size )
	print( "Labels: ", label_encoder.classes_ )
	print( "-" * 30 )
	print( "Data dimensions: ", X_train.shape, y_train.shape )
	print( "Data samples: ")
	print( X_train[:2] )
	print( y_train[:2] )
	print( "-" * 30 )

	if return_as_sentence:
		dataset = Dataset( train, test, vocab, max_sentence_size, labels_encoder=label_encoder, dim_embeds=100, embeddings_matrix=embeddings_weight_matrix )
	else:
		dataset = Dataset( train, test, vocab, -1, labels_encoder=label_encoder, dim_embeds=100, embeddings_matrix=embeddings_weight_matrix )
	return dataset






	




	
if __name__ == "__main__":
	load_snt_by_sentences( "data/data_set", keep_prob=0, embeds_file=None )
	#pass
