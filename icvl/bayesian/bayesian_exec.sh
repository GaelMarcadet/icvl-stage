############################"
# file: bayesian_exec.sh
# description: Executes bayesian script
############################

# defining data to load
DATASET="../../data/IOBCorpus"
EXTENSION="snt"

# executes the bayesian python script
# with params defined above
python3 bayesian.py \
	--directory $DATASET \
	--extension $EXTENSION