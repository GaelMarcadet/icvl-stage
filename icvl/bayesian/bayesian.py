#########################################""
# File: bayesian.py
# Author: Gael Marcadet
# Description: Bayesian models trainer
########################################

from sys import argv
from sklearn.naive_bayes import MultinomialNB, BernoulliNB, CategoricalNB, ComplementNB, GaussianNB
from sklearn.metrics import accuracy_score, recall_score, f1_score
from icvl.utils.v2.core import load_data_as_words, Dataset
from icvl.utils.v2.metrics import summary_by_label
from icvl.utils.v2.user_interface import create_parser_to_choose_dataset

def evaluate_bayesian_model( dataset : Dataset, model ):
	def _tranpose_input( data ):
		return data.reshape(-1, 1)

	# the first step is to train the model
	model.fit( _tranpose_input( dataset.train[0] ), dataset.train[1] )

	# then we are ready to ask the model to predict class
	X_test_transposed = _tranpose_input( dataset.test[0] )
	y_pred = model.predict( X_test_transposed )
	y_true = dataset.test[1]

	# finally display score results
	model_name = type( model ).__name__
	global_accuracy = accuracy_score( y_true, y_pred )
	global_recall = recall_score( y_true, y_pred, average="micro" )
	global_f1 = f1_score( y_true, y_pred, average="micro" )
	labels_metrics = summary_by_label( y_true, y_pred, n_class=len(dataset.labels_encoder.classes()) )
	
	print( f"* Result for {model_name} model" )

	print( f"\t* Global metrics" )
	print( f"\t\t- Accuracy:\t", global_accuracy )
	print( f"\t\t- Recall:\t", global_recall )
	print( f"\t\t- F-Mesure:\t", global_f1 )

	print( f"\t* Metrics by label" )
	for label, ( label_acc, label_rec, label_f1 ) in labels_metrics:
		label_name = dataset.labels_encoder.invert_transform_single_token( label )
		print( f"\t\t- {label_name}: Accuracy = {label_acc}, Recall = {label_rec}, F1 = {label_f1}" )

	print( "-" * 30 )

if __name__ == "__main__":
	argv = argv[1:]
	if argv == []:
		dataset = "data/BNFCorpus"
		extension = "txt"
		normalize = False
	else:
		parser = create_parser_to_choose_dataset()
		args = parser.parse_args()
		dataset = args.directory
		extension = args.extension
		normalize = args.normalize
	
	print( f"* Corpus directory set as {dataset} and extension as {extension}" )
	dataset = load_data_as_words( dataset, files_extension=extension, normalize=normalize )
	dataset.summary()

	evaluate_bayesian_model( dataset, GaussianNB() )
	evaluate_bayesian_model( dataset, BernoulliNB() )
	evaluate_bayesian_model( dataset, ComplementNB() )
	evaluate_bayesian_model( dataset, MultinomialNB() )
	evaluate_bayesian_model( dataset, CategoricalNB() )

	
